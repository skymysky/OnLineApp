package com.example.administrator.OnLine;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class StarActivity extends AppCompatActivity {
    private EditText editTextUserId,editTextOtherId;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        jianCha();
        setContentView(R.layout.activity_star);
        initView();
        listener();
        initpermiss();
    }
    /**
     * 检查是否登录过
     */
    private void jianCha(){
        //本地数据
        SharedPreferences sharedPreferences=getApplication().getSharedPreferences("user",
                Activity.MODE_PRIVATE);
        int online=sharedPreferences.getInt("online",0);
        if(online==1){
            //跳转屏幕
            Intent intent=new Intent(StarActivity.this,MainActivity.class);
            startActivity(intent);
            this.finish();
        }
    }
    /**
     * 请求定位权限
     */
    private void initpermiss(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},0x01);
        }
    }
    /**
     * 初始化控件
     */
    private void initView(){
        editTextOtherId=(EditText)findViewById(R.id.otherId);
        editTextUserId=(EditText)findViewById(R.id.userId);
        button=(Button)findViewById(R.id.starOnline);
    }
    /**
     * 设置监听器
     */
    private void listener(){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(StarActivity.this)
                        .setIcon(R.mipmap.ico)
                        .setTitle("OnLine")
                        .setMessage("\t是否确认对方和自己的ID都输入正确了呢？")
                        .setPositiveButton("我已确认无误", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                playMain();
                            }
                        })
                        .setNegativeButton("返回看看", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(StarActivity.this,"操作取消",Toast.LENGTH_SHORT).show();
                            }
                        })
                        .show();
            }
        });
    }

    /**
     * 跳转到MainActivity
     */
    private void playMain(){
        String userId=editTextUserId.getText().toString();
        String otherId=editTextOtherId.getText().toString();
        if(userId.length()<5){
            diglog("提示","用户名输入不可少于5个字符");
        }else if(otherId.length()<5){
            diglog("提示","对方用户名不可少于5个字符");
        }else{
            //本地数据
            SharedPreferences sharedPreferences=getApplication().getSharedPreferences("user",
                    Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("online",1);
            editor.putString("userid",userId);
            editor.putString("otherid",otherId);
            editor.commit();
            //跳转屏幕
            Intent intent=new Intent(StarActivity.this,MainActivity.class);
            startActivity(intent);
            this.finish();

        }
    }
    /**
     * DigLog通用方法
     */
    private void diglog(String title,String message){
        new AlertDialog.Builder(StarActivity.this)
                .setIcon(R.mipmap.ico)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }
}
